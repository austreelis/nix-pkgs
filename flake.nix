{
  description = "A collection of packages";

  inputs = {
    nixpkgs.url = "nixpkgs";
  };

  outputs = { self, nixpkgs } @ inputs: let
    systems = [
      "x86_64-linux"
    ];
    mkOutputs = system: let
      pkgs = nixpkgs.lib.recursiveUpdate nixpkgs (import nixpkgs {
        inherit system;
      });

      customPkgs = pkgs.lib.mapAttrs (_: f: import f inputs) {
        vim-custom = ./apps/vim-custom.nix;
        mindustryFHS = ./games/mindustry-fhs.nix;
        sloth = ./apps/sloth.nix;
      };
    in {
      lib = nixpkgs.lib.extend (final: prev: {
        maintainers = prev.maintainers // {
          austreelis = {
            email = "dev@austreelis.net";
            github = "Austreelis";
            githubId = 56743515;
            name = "Austreelis Swhaele";
          };
        };
      });
    
      packages.${system} = {
        mindustry-alpha = pkgs.callPackage customPkgs.mindustryFHS { };
        vim-custom = pkgs.callPackage customPkgs.vim-custom { };
        sloth = pkgs.callPackage customPkgs.sloth { };
      };
    };
  in with nixpkgs.lib; foldl' recursiveUpdate { } (map mkOutputs systems);
}
