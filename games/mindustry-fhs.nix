{ ... }:
{ buildFHSUserEnv, fetchurl, lib
, glew
, jre
, mindustryJar ? fetchurl {
  url = "https://github.com/Anuken/Mindustry/releases/download/v${mindustryVersion}/Mindustry.jar";
  sha256 = "sha256-YrPB+ZtKhugUguTIFIr0Tqq+OwMbMxVklBSyCK0pb/A=";
}
, mindustryVersion ? "135"
}: buildFHSUserEnv {
  name = "mindustry";
  targetPkgs = _: [
    glew
  ];
  runScript = "${jre}/bin/java -jar ${mindustryJar}";
}
