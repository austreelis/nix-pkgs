# An opiniated pre-configured vim that fits *my* need
# You probably want to only take inspiration, not use it unmodified
{ ... }:
{ vim_configurable,
  vimPlugins,
  git,
  python3,
  ...
}:
vim_configurable.customize {
  name = "vim";

  vimrcConfig.packages.myplugins = with vimPlugins; {
    start = [
      airline
      fugitive
      fugitive-gitlab-vim
      nerdtree
      vim-airline-themes
      vim-nerdtree-syntax-highlight
      vim-nix
    ];
    opt = [
      nerdtree-git-plugin
      vim-devicons
    ];
  };

  vimrcConfig.customRC = ''

    " Load plugins that have to be loaded in a given order

    packadd nerdtree-git-plugin
    packadd vim-devicons

    " Basic behavior

    syntax on
    filetype on

    scriptencoding utf-8
    set encoding=utf-8
    set fileencoding=utf-8

    set nocompatible               " allow breaking vi behavior
    set nomodeline

    set mouse=a                    " allow using mouse in all modes
    set ttymouse=sgr               " alacritty-specific fix, see https://github.com/alacritty/alacritty/issues/803
    set backspace=indent,start,eol

    set autoindent
    set smartindent
    set ignorecase
    set smartcase

    set incsearch

    set number
    set scrolloff=5

    let mapleader = '`'

    " Decorations

    set ruler           " show ruler
    set laststatus=2    " always show status line
    set showtabline=2   " always show tab line
    set guioptions -=e  " prefer text tab line rather than gui one
    set shortmess-=S    " show search index

    " Remappings

    " swapping : and ;
    nnoremap ; :
    nnoremap : ;

    " Blinking current search match
    function! BlinkCurrentMatch (group, blinks, blinktime)
      let [bufnum, lnum, col, off] = getpos('.')
      let matchlen = strlen(matchstr(strpart(getline('.'), col-1), @/))
      let target_pat = '\c\%#'.@/
      for n in range(1, a:blinks)
        let mat = matchadd(a:group, target_pat, 101)
        redraw
        exec 'sleep' . float2nr(a:blinktime / (2*a:blinks) * 1000) . 'm'
        call matchdelete(mat)
        redraw
        exec 'sleep' . float2nr(a:blinktime / (2*a:blinks) * 1000) . 'm'
      endfor
    endfunction
    highlight ReversedSearch ctermbg=Black ctermfg=DarkMagenta
    nnoremap <silent> n n:call BlinkCurrentMatch('ReversedSearch', 5, 0.4)<cr>
    nnoremap <silent> N N:call BlinkCurrentMatch('ReversedSearch', 5, 0.4)<cr>

    " Custom mappings

    " Visual mode pressing * or # searches for the current selection
    " from https://github.com/andrewrk/dotfiles/blob/master/.nixpkgs/config.nix
    vnoremap <silent> * :call VisualSelection('f')<cr>
    vnoremap <silent> # :call VisualSelection('b')<cr>

    nnoremap <silent> <BS> :noh<cr>

    " Window navigation
    map <C-h> <C-W>h
    map <C-j> <C-W>j
    map <C-k> <C-W>k
    map <C-l> <C-W>l

    " Tabs
    noremap <leader>tn :tabnew<cr>
    noremap <leader>td :tabclose<cr>
    noremap <leader>te :tab drop<space>

    " Buffers
    noremap H :bp<cr>
    noremap L :bn<cr>
    noremap D :bd<cr>

    " Toggle paste mode
    noremap <leader>v :setlocal paste!<cr>

    " Indentation

    set expandtab
    set tabstop=8
    set shiftwidth=2
    set softtabstop=-1 " use shiftwidth value
    set shiftround

    " Text wrapping

    set textwidth=0
    set wrap
    set linebreak
    set showbreak=»»

    " Characters listing

    set list
    set listchars=tab:├─┤,trail:·,extends:▶,precedes:◀,conceal:▪

    " Tabs

    set tabpagemax=500 " Maximum of tabs opened by -p option

    " Highlighting

    set hlsearch
    set background=dark " get sensible default highlights for dark terminal
    highlight Cursor      cterm=NONE   ctermbg=Black       ctermfg=White
    highlight IncSearch   cterm=bold   ctermbg=Magenta     ctermfg=Black
    highlight Search      cterm=NONE   ctermbg=DarkMagenta ctermfg=Black
    highlight Visual      cterm=NONE   ctermbg=Magenta     ctermfg=Black
    highlight link VisualNOS Visual

    " Custom highlights

    match Error /\%81v/

    " Airline

    let g:airline#extensions#tabline#enabled = 1
    let g:airline#extensions#tabline#formatter = 'unique_tail'
    let g:airline#extensions#tabline#left_sep = '|'
    let g:airline#extensions#tabline#right_sep = '|'
    let g:airline#extensions#tabline#left_alt_sep = '|'
    let g:airline#extensions#tabline#right_alt_sep = '|'

    " Use custom theme based on dark
    " weirdly, the dark palette seems to be created only after sourcing the
    " vimrc, but before VimEnter
    let g:airline_theme = 'dark'
    autocmd VimEnter *  let g:airline#themes#custom#palette = copy(g:airline#themes#dark#palette)
    autocmd VimEnter *  let g:airline#themes#custom#palette.commandline = copy(g:airline#themes#dark#palette.normal)
    autocmd VimEnter *  let g:airline#themes#custom#palette.commandline_modified = copy(g:airline#themes#dark#palette.normal_modified)
    autocmd VimEnter *  let g:airline#themes#custom#palette.normal = copy(g:airline#themes#dark#palette.commandline)
    " Actually set the theme
    autocmd VimEnter *  let g:airline_theme = 'custom'

    let g:airline_symbols = {}
    let g:airline#extensions#branch#prefix = '⤴,➔, ➥, ⎇'
    let g:airline#extensions#readonly#symbol = ''
    let g:airline#extensions#linecolumn#prefix = '¶'
    let g:airline#extensions#paste#symbol = 'ρ'
    let g:airline_symbols.paste = 'ρ'
    let g:airline_symbols.whitespace = 'Ξ'
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ' '
    " watch out for the escaped double single quotes (nix syntax)
    let g:airline_symbols.colnr = '''

    " Nerd-tree

    nnoremap <leader>n :NERDTreeToggle<cr>
    let g:NERDTreeGitStatusIndicatorMapCustom = {
      \ 'Modified'  :'✹',
      \ 'Staged'    :'✚',
      \ 'Untracked' :'✭',
      \ 'Renamed'   :'➜',
      \ 'Unmerged'  :'═',
      \ 'Deleted'   :'✖',
      \ 'Dirty'     :'✗',
      \ 'Ignored'   :'☒',
      \ 'Clean'     :'✔︎',
      \ 'Unknown'   :'?',
      \ }
    let g:NERDTreeGitStatusShowIgnored = 1 " may alter performance
    let g:NERDTreeGitStatusShowClean = 1
    let g:NERDTreeGitStatusGitBinPath = '${git}/bin/git'
    "" start NERDTree when Vim is started without file arguments.
    autocmd StdinReadPre * let s:std_in = 1
    autocmd VimEnter *  if argc() == 0 && !exists('s:std_in')
    autocmd VimEnter *    NERDTree
    autocmd VimEnter *  endif
  '';
}
