{ ... }:
{
  lib,
  # derivation deps
  stdenv, fetchFromGitHub,
  # derivation params
  version ? "c1c0ee9",
  slothSrc ? fetchFromGitHub {
    owner = "lunasorcery";
    rev = version;
    repo = "sloth";
    hash = "sha256-SEyGRtnh48YgjoIeU+iXqMTKUw9SM8wzWZoSau9FQK4=";
  },
  ...
}: stdenv.mkDerivation {
  pname = "sloth";
  inherit version;

  src = slothSrc;

  dontConfigure = true;

  installPhase = ''
    mkdir -p $out
    chmod +x bin/*
    cp -rT bin $out/bin
  '';

  meta = {
    homepage = "https://github.com/lunasorcery/sloth";
    description = "Like cat, but slower";
    longDescription = builtins.readFile "${slothSrc}/README.md";
    branch = "main";
    mainProgram = "sloth";
    license = lib.licenses.unfree;
    maintainers = [ lib.maintainers.austreelis ];
  };
}
